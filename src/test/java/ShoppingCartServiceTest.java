import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import models.ItemType;
import service.impl.ShoppingCartServiceImpl;


public class ShoppingCartServiceTest {
    private ShoppingCartServiceImpl cartService;

    @BeforeEach
    public void setup() {
        this.cartService = new ShoppingCartServiceImpl();
    }

    @Test
    void whenCalculateDiscountThenSuccess() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = ShoppingCartServiceImpl.class.getDeclaredMethod("calculateDiscount", ItemType.class, int.class);
        method.setAccessible(true);

        //Case 1
        int expectedResult = 0;
        int actualResult = (int) method.invoke(cartService, ItemType.NEW, 2);
        Assertions.assertEquals(expectedResult, actualResult);
        //Case 2
        expectedResult = 10;
        actualResult = (int) method.invoke(cartService, ItemType.REGULAR, 100);
        Assertions.assertEquals(expectedResult, actualResult);
        //Case 3
        expectedResult = 80;
        actualResult = (int) method.invoke(cartService, ItemType.SECOND_FREE, 1000);
        Assertions.assertEquals(expectedResult, actualResult);
        //Case 4
        expectedResult = 0;
        actualResult = (int) method.invoke(cartService, ItemType.SECOND_FREE, 0);
        Assertions.assertEquals(expectedResult, actualResult);
        //Case 5
        expectedResult = 70;
        actualResult = (int) method.invoke(cartService, ItemType.SALE, 0);
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void whenAppendFormattedThenSuccess() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = ShoppingCartServiceImpl.class.getDeclaredMethod("appendFormatted",
                StringBuilder.class, String.class, int.class, int.class);
        method.setAccessible(true);

        StringBuilder stringBuilder = new StringBuilder();
        String testString = "testString";

        //Case 1
        method.invoke(cartService, stringBuilder, testString, 0, 4);
        String expectedResult = "test ";
        String actualResult = stringBuilder.toString();
        Assertions.assertEquals(expectedResult, actualResult);
        //Case 2
        method.invoke(cartService, stringBuilder, testString, -1, 20);
        expectedResult = "test testString           ";
        actualResult = stringBuilder.toString();
        Assertions.assertEquals(expectedResult, actualResult);
        //Case 3
        method.invoke(cartService, stringBuilder, testString, 23, 4);
        expectedResult = "test testString           test ";
        actualResult = stringBuilder.toString();
        Assertions.assertEquals(expectedResult, actualResult);
    }
}
