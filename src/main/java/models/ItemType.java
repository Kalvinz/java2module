package models;

public enum ItemType {
    NEW, REGULAR, SECOND_FREE, SALE
}
