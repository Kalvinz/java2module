package models;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCart {
    private List<Item> items = new ArrayList<>();

    /**
     * Adds new item.
     *
     * @param title    item title 1 to 32 symbols
     * @param price    item ptice in USD, > 0
     * @param quantity item quantity, from 1
     * @param type     item type
     * @throws IllegalArgumentException if some value is wrong
     */
    public void addItem(String title, double price, int quantity, ItemType type) {
        if (title == null || title.length() == 0 || title.length() > 32) {
            throw new IllegalArgumentException("Illegal title");
        }
        if (price < 0.01) {
            throw new IllegalArgumentException("Illegal price");
        }
        if (quantity <= 0) {
            throw new IllegalArgumentException("Illegal quantity");
        }
        items.add(Item.builder()
                .title(title)
                .price(price)
                .quantity(quantity)
                .type(type)
                .build());
    }
}
