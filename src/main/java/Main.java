import models.ItemType;
import models.ShoppingCart;
import service.api.ShoppingCartService;
import service.impl.ShoppingCartServiceImpl;


public class Main {
    private static final ShoppingCartService shoppingCartService = new ShoppingCartServiceImpl();

    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem("Apple", 0.99, 5, ItemType.NEW);
        cart.addItem("Banana", 20.00, 4, ItemType.SECOND_FREE);
        cart.addItem("A long piece of toilet paper", 17.20, 1, ItemType.SALE);
        cart.addItem("Nails", 2.00, 500, ItemType.REGULAR);
        System.out.println(shoppingCartService.formatTicket(cart));
    }
}