package service.api;

import models.ShoppingCart;

public interface ShoppingCartService {
    String formatTicket(ShoppingCart shoppingCart);
}
